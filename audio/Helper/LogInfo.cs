﻿using System;
using System.IO;
using System.Threading.Tasks;
using log4net;

namespace audio.Helper
{
    /// <summary>
    /// 日志
    /// </summary>
    public class LogInfo  :ILog
    {

        private readonly log4net.ILog _logger;
        public LogInfo(string filePath, string name)
        {
            FileInfo configFile = new FileInfo(filePath);
            log4net.Config.XmlConfigurator.Configure(configFile);
            _logger = LogManager.GetLogger(name);
        }
        public async void Debug(object message)
        {
            // _logger.Debug(message);
            await Task.Run(() =>
            {
                _logger.Debug(message);
            });
        }
        public async void Error(object message)
        {
            //   _logger.Error(message);
            await Task.Run(() =>
            {
                _logger.Error(message);
            });
        }

        public async void Info(string title, string message)
        {
            await Task.Run(() =>
            {
                _logger.Info(title + " " + message);
            });
        }
        public static string GetHexLog(byte[] data)
        {
            if (data == null) return "";
            string result = "";
            int start = 0;
            while (start < data.Length)
            {
                if (!result.Equals("")) result += " ";
                int len = data.Length - start;
                if (len > 16) len = 16;
                string strByte = BitConverter.ToString(data, start, len);
                strByte = strByte.Replace('-', ' ');
                //if (len > 8)
                //{
                //    strByte = strByte.Substring(0, 23) + "-" + strByte.Substring(24);
                //}
                //if (len < 16)
                //{
                //    strByte += new string(' ', (16 - len) * 3);
                //}
                //strByte += "  ";
                //for (int i = 0; i < len; i++)
                //{
                //    char c = Convert.ToChar(data[start + i]);
                //    if (char.IsControl(c))
                //    {
                //        c = '.';
                //    }
                //    strByte += c;
                //}
                result += strByte;
                start += len;
            }
            return result;
        }



    }
}
