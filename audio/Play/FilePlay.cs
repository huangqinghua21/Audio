﻿using System;
using System.IO;
using System.Threading;
using audio.Helper;
using NAudio.Wave;

namespace audio.Play
{
    public class FilePlay
    {


        public void Play(string path)
        {
            ThreadPool.QueueUserWorkItem(StreamMp3, path);
        }

        readonly ILog _log = new LogInfo("log4net.config", "");
        bool _fileState;
        readonly AudioPlay _audioPlay = new AudioPlay();
        private void StreamMp3(object path)
        {
            _fileState = true;
            MemoryStream ms;
            using (FileStream fs = File.OpenRead((string)path))
            {
                int length = (int)fs.Length;
                byte[] data = new byte[length];
                fs.Position = 0;
                fs.Read(data, 0, length);
                ms = new MemoryStream(data);
            }
            do
            {
                Mp3Frame frame = Mp3Frame.LoadFromStream(ms);

                if (frame == null)
                {
                    _fileState = false;
                }
                else
                {
                    _log.Info(@"数量:" + frame.RawData.Length + "  ", LogInfo.GetHexLog(frame.RawData));
                    _audioPlay.StreamMp3File(frame);
                }
            } while (_fileState);



        }
    }
}
